import React, {Component} from 'react';
import axios from 'axios';
import { API_JOB_URL } from './API';

export default class PostJob extends Component {

    constructor(props) {
        super(props);
        this.state = {
            jobDescription: '',
            jobResponsible: '',
            jobPrice: '',
            jobStats: false
        }
    }

    onChangeJobDescription = (e) => {
        this.setState({
            jobDescription: e.target.value
        });
    }

    onChangeJobResponsible = (e) => {
        this.setState({
            jobResponsible: e.target.value
        });
    }

    onChangeJobPrice = (e) => {
        this.setState({
            jobPrice: e.target.value
        });
    }

    onSubmit = (e) => {
        e.preventDefault();
        const newjob = {
            jobDescription: this.state.jobDescription,
            jobResponsible: this.state.jobResponsible,
            jobPrice: this.state.jobPrice,
            jobStats: this.state.jobStats
        }

        axios.post(API_JOB_URL+'add', newjob)
            .then(res => console.log(res.data));

        this.setState({
            jobDescription: '',
            jobResponsible: '',
            jobPrice: '',
            jobStats: false
        })
    }

    render() {
        let {
            jobDescription, 
            jobResponsible,
            jobPrice,
            jobStats
        } = this.state

        return (
            <div style={{marginTop: 20}}>
                <h3>Post a New Job</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Description: </label>
                        <input  type="text"
                            className="form-control"
                            value={jobDescription}
                            onChange={this.onChangeJobDescription}
                        />
                    </div>
                    <div className="form-group">
                        <label>Skills: </label>
                        <input  type="text"
                            className="form-control"
                            value={jobResponsible}
                            onChange={this.onChangeJobResponsible}
                        />
                    </div>
                    <div className="form-group">
                        <label>Price: </label>
                        <input  type="text"
                            className="form-control"
                            value={jobPrice}
                            onChange={this.onChangeJobPrice}
                        />
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Post a job" className="btn btn-primary" />
                    </div>
                </form>
            </div>
        )
    }
}