import React, { Component } from 'react'
import { Link } from 'react-router-dom';

class Job extends Component{
    constructor(props){
        super(props)
    }
    render(){
        var props = this.props
        return(
            <tr>
                <td className={props.job.jobStats ? 'applied' : ''}>
                    {props.job.jobDescription}
                </td>
                <td className={props.job.jobStats ? 'applied' : ''}>
                    {props.job.jobResponsible}</td>
                <td className={props.job.jobStats ? 'applied' : ''}>
                    {props.job.jobPrice}
                </td>
                <td>
                    {
                        props.job.jobStats ?
                        <Link to={"/edit/"+props.job._id}>Rtract</Link>
                        :
                        <Link to={"/edit/"+props.job._id}>Apply</Link>
                    }
                </td>
            </tr>    
        )        
    }
}
export default Job
