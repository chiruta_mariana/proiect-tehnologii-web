import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { API_JOB_URL as API_Link } from './API';
import { API_APPLICANT_URL } from './API';

export default class ApplyJob extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username:'',
            jobDescription: '',
            jobResponsible: '',
            jobPrice: '',
            jobStats: false
        }
    }

    componentDidMount() {
        axios.get(API_Link+this.props.match.params.id)
            .then(response => {
                this.setState({
                    jobDescription: response.data.jobDescription,
                    jobResponsible: response.data.jobResponsible,
                    jobPrice: response.data.jobPrice,
                    jobStats: response.data.jobStats
                })
                axios.get(API_APPLICANT_URL+this.props.match.params.id)
                .then(res1 => {
                    console.log(res1.data)
                    if(res1.data.applicant)
                        this.setState({username:res1.data.applicant.applicants});
                })
            })
            .catch(function(error) {
                console.log(error)
            })
    }

    onChangeUserName = (e) => {
        this.setState({
            username: e.target.value
        })
    }
    onChangeJobDescription = (e) => {
        this.setState({
            jobDescription: e.target.value
        });
    }

    onChangeJobResponsible = (e) => {
        this.setState({
            jobResponsible: e.target.value
        });
    }

    onChangeJobPrice = (e) => {
        this.setState({
            jobPrice: e.target.value
        });
    }

    onChangeJobApply = (e) => {
        this.setState({
            jobStats: true
        });
        const obj = {
            jobDescription: this.state.jobDescription,
            jobResponsible: this.state.jobResponsible,
            jobPrice: this.state.jobPrice,
            jobStats: true
        };
        const applicant = {
            jobID:this.props.match.params.id,
            applicants: this.state.username
        }
        axios.post(API_Link+'update/'+this.props.match.params.id, obj)
            .then(res => {
                console.log(res.data)
                //this.props.history.push('/');
                axios.post(API_APPLICANT_URL+'add', applicant)
                .then(res1=>{
                    console.log(res1.data)
                    this.props.history.push('/');
                })
            });
    }

    onChangeJobRetract = (e) => {
        this.setState({
            jobStats: false
        });
        const obj = {
            jobDescription: this.state.jobDescription,
            jobResponsible: this.state.jobResponsible,
            jobPrice: this.state.jobPrice,
            jobStats: false
        };
        console.log(obj)
        axios.post(API_Link+'update/'+this.props.match.params.id, obj)
            .then(res => {
                console.log(res.data)
                axios.delete(API_APPLICANT_URL+this.props.match.params.id)
                .then(res => {
                    console.log(res.data)
                    this.props.history.push('/');
                })
            });
    }

    render() {
        let {
            username, 
            jobDescription, 
            jobResponsible,
            jobPrice,
            jobStats
        } = this.state
        return (
            <div>
                <h3>Apply Job</h3>
                <div className="form-group">
                    <label>Description: </label> <br/>
                    <label>{jobDescription}</label>
                </div>
                <div className="form-group">
                    <label>Skills: </label> <br/>
                    <label>{jobResponsible}</label>
                </div>
                <div className="form-group">
                    <label>Price: </label> <br/>
                    <label>{jobPrice}</label>
                </div>

                {
                    jobStats &&
                    <div className="form-group">
                        <label>Name: </label> <br/>
                        <label>{this.state.username}</label> <br/>
                        <button className="btn btn-primary" style={{marginRight:20}} onClick={this.onChangeJobRetract}>Retract</button>
                    </div>
                }
                {
                    !jobStats &&
                    <div className="form-group">
                        <label>Input your Name: </label>
                        <input  type="text"
                            className="form-control"
                            value={username}
                            onChange={this.onChangeUserName}
                        /> <br/>
                        <button className="btn btn-primary" style={{marginRight:20}} onClick={this.onChangeJobApply}>ApplyJob</button>
                    </div>
                }
                <Link className="btn btn-danger" to="/">Cancel</Link>
            </div>
        )
    }
}