import React, {Component} from 'react';
import axios from 'axios';
import Job from './Job';
import { API_JOB_URL } from './API';

export default class jobsList extends Component {

    constructor(props) {
        super(props);
        this.state = {jobs: []};
    }

    componentDidMount() {
        axios.get(API_JOB_URL)
            .then(response => {
                this.setState({jobs: response.data});
            })
            .catch((error) => {
                console.log(error);
            })
    }

    componentDidUpdate() {
        axios.get(API_JOB_URL)
        .then(response => {
            this.setState({jobs: response.data});
        })
        .catch((error) => {
            console.log(error);
        })   
    }

    render() {
        let {jobs} = this.state;
        return (
            <div>
                <h3>Jobs List</h3>
                <table className="table table-striped" style={{ marginTop: 20 }}>
                    <thead>
                        <tr>
                            <th>Description</th>
                            <th>Skills</th>
                            <th>Price</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        { 
                            jobs.map((job, i) => {
                                return <Job job={job} key={i} />;
                            }) 
                        }
                    </tbody>
                </table>
            </div>
        )
    }
}