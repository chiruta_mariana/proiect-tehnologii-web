import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import PostJob from "./components/PostJob";
import ApplyJob from "./components/ApplyJob";
import JobList from "./components/JobList";

class App extends Component {
  render() {
    return (
      <Router>
        <div className="container">
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="collpase nav-collapse">
              <ul className="navbar-nav mr-auto">
                <li className="navbar-item">
                  <Link to="/" className="nav-link">Job List</Link>
                </li>
                <li className="navbar-item">
                  <Link to="/create" className="nav-link">Post Job</Link>
                </li>
              </ul>
            </div>
          </nav>

          <Route path="/" exact component={JobList} />
          <Route path="/edit/:id" component={ApplyJob} />
          <Route path="/create" component={PostJob} />
        </div>
      </Router>
    );
  }
}

export default App;
